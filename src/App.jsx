import React from 'react';
import './App.scss';
import HeaderBlock  from './components/HeaderBlock/HeaderBlock.jsx';
import AboutBlock from "./components/AboutBlock/AboutBlock.jsx";
import AdvantageBlock from "./components/AdvantageBlock/AdvantageBlock.jsx";
import ScrinsBlock from "./components/ScrinsBlock/ScrinsBlock.jsx";
import ReviewsBlock from "./components/ReviewsBlock/ReviewsBlock.jsx";
import OffersBlock from "./components/OffersBlock/OffersBlock.jsx";
import FooterBlock from "./components/FooterBlock/FooterBlock.jsx";




function App() {

  return (
    <>
      <HeaderBlock />
      <AboutBlock />
      <AdvantageBlock />
      <ScrinsBlock />
      <ReviewsBlock />
      <OffersBlock />
      <FooterBlock />
    </>
  );
};

export default App;
