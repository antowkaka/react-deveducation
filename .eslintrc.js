module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 11,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "prettier/react": [
            "error",
            {
                "bracketSpacing": true,
                "jsxBracketSameLine": false,
                "jsxSingleQuote": false,
                "printWidth": 100,
                "semi": true,
                "singleQuote": true,
                "tabWidth": 2,
                "trailingComma": "all",
                "useTabs": false,
                "endOfLine": "lf"
            }
        ],
        "react/jsx-uses-react": "error",
        "react/jsx-uses-vars": "error"
    }
};
