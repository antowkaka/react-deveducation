const path = require('path');
const fs = require('fs');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const  HtmlWebpackPlugin  = require('html-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


const ROOT_DIR = fs.realpathSync(process.cwd());

const analyze = process.argv.includes('--analyze');

function pathResolve(...args) {
	return path.resolve(ROOT_DIR, ...args)
}

function getName(ext) {
	return `[name].[${isDev ? 'hash:8' : 'contenthash'}].bundle.${ext}`
}


const isDev = process.env.NODE_ENV !== 'production';

module.exports = {
	mode: isDev ? 'development' : 'production',
	entry: './src/index.jsx',
	output: {
		filename: getName('js'),
		path: pathResolve('dist')
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: [
					'babel-loader'
				]
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'sass-loader',
				],
			},
			{
				test: /\.(png|jpe?g|gif|svg)$/i,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: getName('[ext]'),
							outputPath: 'img'
						}
					},
				],
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: getName('[ext]'),
							outputPath: 'fonts'
						}
					},
				],
			}
		],
	},
	plugins: [
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			favicon: './public/favicon.ico',
			template: './public/index.html'
		}),
		new MiniCssExtractPlugin({
			filename: getName('css')
		}),
		analyze && new BundleAnalyzerPlugin()
	].filter(Boolean),
	devtool: 'source-map',
	devServer: {
		port: 9000,
		open: true
	}
};