import React from "react";
import './AboutBlock.scss';


function AboutBlock() {
	const paragraph_texts = [
		'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis facilis fuga, illo at. Natus eos, eligendi illum rerum omnis porro ex, magni, explicabo veniam incidunt in quam sapiente ut ipsum.',
		'Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde perferendis nemo debitis dolor.'
	];

	return (
		<div className="About_wrap">
			<div className="About container is-fluid has-text-light">
				<div className="About_text">
					<h3 className="About_text_title title is-3">About your product</h3>
					<p className="About_text_paragraph">{ paragraph_texts[0] }</p>
					<p className="About_text_paragraph">{ paragraph_texts[1] }</p>
				</div>
				<div className="About_image"></div>
			</div>
		</div>
	)
}

export default AboutBlock;