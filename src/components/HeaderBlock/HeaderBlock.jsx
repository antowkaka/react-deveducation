import React from "react";
import './HeaderBlock.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';


function HeaderBlock() {
	const list = [
		'Put on this page information about your product',
		'A detailed description of your product',
		'Tell us about the advantages and merits',
		'Associate the page with the payment system'
	];

	const check = <FontAwesomeIcon icon={faCheck} />;
	return (
		<div className="Header_wrap">
			<div className="Header container is-fluid has-text-light">
				<div className="Header_text">
					<h1 className="Header_text_title title is-1 has-text-light">Product name</h1>
					<ul className="Header_text_list">
						<li className="Header_list_item">
							{check} {list[0]}
						</li>
						<li className="Header_list_item">
							{check} {list[1]}
						</li>
						<li className="Header_list_item">
							{check} {list[2]}
						</li>
						<li className="Header_list_item">
							{check} {list[3]}
						</li>
					</ul>
				</div>
				<div className="Header_image"></div>
			</div>
		</div>
	)
}

export default HeaderBlock;
