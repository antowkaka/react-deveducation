import React from "react";
import './OffersBlock.scss';
import OfferCardComponent from "./OfferCard.component.jsx";

function OffersBlock() {
	const optionsForStandart = [
		'Porro officia cumque sint deleniti;',
	  'Тemo facere rem vitae odit;',
	  'Cum odio, iste quia doloribus autem;',
	  'Aperiam nulla ea neque.'
	];

	const optionsForPremium = [
		'Porro officia cumque sint deleniti;',
		'Тemo facere rem vitae odit;',
		'Cum odio, iste quia doloribus autem;',
		'Aperiam nulla ea neque.',
		'Porro officia cumque sint deleniti;',
		'Тemo facere rem vitae odit;',
		'Cum odio, iste quia doloribus autem;',
		'Aperiam nulla ea neque.'
	];

	const optionsForLux = [
		'Porro officia cumque sint deleniti;',
		'Тemo facere rem vitae odit;',
		'Cum odio, iste quia doloribus autem;',
		'Aperiam nulla ea neque.',
		'Porro officia cumque sint deleniti;',
		'Тemo facere rem vitae odit;',
		'Cum odio, iste quia doloribus autem;',
		'Aperiam nulla ea neque.',
		'Porro officia cumque sint deleniti;',
		'Тemo facere rem vitae odit;',
		'Cum odio, iste quia doloribus autem;',
		'Aperiam nulla ea neque.'
	];

	return (
		<div className="Offers container is-fluid">
			<h3 className="Offers_title title is-3">Buy it now</h3>
			<div className="Offers_cards">
				<OfferCardComponent plan="Standart" price="$100" opts={optionsForStandart}/>
				<OfferCardComponent plan="Premium" price="$150" opts={optionsForPremium}/>
				<OfferCardComponent plan="Lux" price="$200" opts={optionsForLux}/>
			</div>
		</div>
	)
}

export default OffersBlock;