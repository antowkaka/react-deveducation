import React from "react";
import './OfferCard.scss';

class OfferCardComponent extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			plan: props.plan,
			price: props.price,
			options: props.opts
		}
	}


	render() {
		return (
			<div className="OfferCard">
				<header className="OfferCard_header">
					<h3 className="OfferCard_header_title title is-3 has-text-light">{ this.state.plan }</h3>
					<div className="OfferCard_header_price title is-2">{ this.state.price }</div>
				</header>
				<div className="OfferCard_content">
					<ul className="OfferCard_content_list">
						{ this.state.options.map((opt, index) => {
							return (
								<li className="has-text-light">{index+1}. {opt}</li>
							)
						})
						}
					</ul>
					<input className="OfferCard_content_btn button" type="submit" value="BUY" />
				</div>
			</div>
		)
	}
}

export default OfferCardComponent;