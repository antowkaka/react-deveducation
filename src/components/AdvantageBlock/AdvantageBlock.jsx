import React from "react";
import './AdvantageBlock.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusSquare } from "@fortawesome/free-solid-svg-icons";


function AdvantageBlock() {

	const advantages = [
		'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime, quaerat porro totam, dolore minus inventore.',
		'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime, quaerat porro totam, dolore minus inventore.',
		'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime, quaerat porro totam, dolore minus inventore.',
		'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime, quaerat porro totam, dolore minus inventore.',
		'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime, quaerat porro totam, dolore minus inventore.',
		'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime, quaerat porro totam, dolore minus inventore.'
	];

	const plusIcon = <FontAwesomeIcon icon={faPlusSquare} size="2x"/>

	return (
		<div className="Advantage_wrap">
			<div className="Advantage container is-fluid has-text-light">
				<h3 className="Advantage_title title is-3">Dignity and pluses product</h3>
				<ul className="Advantage_list">
					{ advantages.map(el => {
							return (
								<li className="Advantage_list_item">
									<div className="Advantage_list_item_icon">{plusIcon}</div> {el}
								</li>
							)
						})
					}
				</ul>
			</div>
		</div>
	)
}

export default AdvantageBlock;