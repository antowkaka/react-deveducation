import React from "react";
import './ScrinsBlock.scss';

function ScrinsBlock() {

	const scrinshots = [
		{
			title: 'The description for the image',
			descr: 'Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde perferendis nemo debitis dolor.',
			imgLink: ''
		},
		{
			title: 'The description for the image',
			descr: 'Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde perferendis nemo debitis dolor.',
			imgLink: ''
		},
		{
			title: 'The description for the image',
			descr: 'Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde perferendis nemo debitis dolor.',
			imgLink: ''
		},
		{
			title: 'The description for the image',
			descr: 'Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde perferendis nemo debitis dolor.',
			imgLink: ''
		}
	];


	return (
		<div className="Scrins container is-fluid">
			<h3 className="Scrins_title title is-3">Scrinshots</h3>
			<ul className="Scrins_list">
				{ scrinshots.map(el => {
						return (
							<li className="Scrins_list_item">
								<div className="Scrins_list_item_img" />
								<div className="Scrins_list_item_text">
									<h3 className="Scrins_list_item_text_title title is-4">{el.title}</h3>
									<p className="Scrins_list_item_text_description">{el.descr}</p>
								</div>
							</li>
						)
					})
				}
			</ul>
		</div>
	)
}

export default ScrinsBlock;