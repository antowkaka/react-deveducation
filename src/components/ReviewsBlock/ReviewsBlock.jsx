import React from "react";
import './ReviewsBlock.scss';

function ReviewsBlock() {

	const reviews = [
		{
			author: 'Lourens S.',
			review: 'Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus officiis sapiente necessitatibus commodi consectetur?',
			imgLink: ''
		},
		{
			author: 'Lourens S.',
			review: 'Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus officiis sapiente necessitatibus commodi consectetur?',
			imgLink: ''
		},
		{
			author: 'Lourens S.',
			review: 'Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus officiis sapiente necessitatibus commodi consectetur?',
			imgLink: ''
		},{
			author: 'Lourens S.',
			review: 'Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus officiis sapiente necessitatibus commodi consectetur?',
			imgLink: ''
		}
	];


	return (
		<div className="Reviews_wrap">
			<div className="Reviews container is-fluid">
				<h3 className="Reviews_title title is-3">Reviews</h3>
				<ul className="Reviews_list">
					{ reviews.map(el => {
						return (
							<li className="Reviews_list_item">
								<div className="Reviews_list_item_img" />
								<div className="Reviews_list_item_review">
									<p className="Reviews_list_item_review_text">{el.review}</p>
									<p className="Reviews_list_item_review_author">{el.author}</p>
								</div>
							</li>
						)
					})
					}
				</ul>
			</div>
		</div>
	)
}

export default ReviewsBlock;